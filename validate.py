'''Compiles/ Validate functions
'''
import json
import jsonschema
import uuid
import datetime

ADD_TASK_SCHEMA = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'args': {
            'type': 'array',
            'items': {'type': 'string'}
        },
        'priority': {'enum': ['NORMAL', 'HIGH', 'URGENT']},
        'next': {
            'type': 'array',
            'items': {'type': 'integer', 'minimum': 0},
        },
        'status': {'enum': ['Completed', 'Error', 'Skipped', 'Working', 'Queued', 'Incomplete']},
        'skip_to': {'type': 'integer', 'minimum': 0},
        'delay': {'type': 'integer', 'minimum': 0},
        'retries': {'type': 'integer', 'minimum': 0},
        'abort_type': {'enum': ['Panic', 'Skip']},
        'immediate_next': {'type': 'boolean'}
    },
    'required': ['name', 'next']
}

ADD_GOAL_SCHEMA = {
    'type': 'object',
    'properties': {
        'id': {'type': 'string'},
        'name': {'type': 'string'},
        'status': {'enum': ['Scheduled', 'Completed', 'Error', 'Idle']},
        'tasks': {
            'type': 'array',
            'items': ADD_TASK_SCHEMA,
            'minItems': 1,
        },
        'time': {'type': 'integer', 'minimum': 0},
        'repeat': {'type': 'integer'},
        'repeat_interval': {'type': 'integer', 'minimum': 0},
        'n_repeated': {'type': 'integer', 'minimum': 0}
    },
    'additionalProperties': False,
    'required': ['name', 'tasks', 'repeat'],
}

UPDATE_GOAL_SCHEMA = {
    **ADD_GOAL_SCHEMA,
    'required': ['name', 'tasks', 'repeat', 'id', 'status', 'n_repeated'],
    'additionalProperties': True,
}


def compile_json(pathname, goal_id=None, update=False):
    '''Validates and transform json file to format accepted by scheduler.
    See add_goal_schema for format
    Args:
        pathname ([string]): pathname of json file
        id([string]): Option id, DO NOT USE unless intended to
    '''
    try:
        with open(pathname, encoding='utf-8') as json_file:
            data = json.load(json_file)

            try:
                if update:
                    jsonschema.validate(instance=data, schema=UPDATE_GOAL_SCHEMA)
                else:
                    jsonschema.validate(instance=data, schema=ADD_GOAL_SCHEMA)
            except jsonschema.exceptions.ValidationError as err:
                print('Goal json validation error')
                print(err.message)
                return None
            except (Exception, ) as err:
                print(f'Unexpected {err=}, {type(err)=}')
                return None
            if goal_id is None and update is False:
                data['id'] = str(uuid.uuid4())
                data['status'] = 'Idle'
                data['n_repeated'] = 0
            for task in data['tasks']:
                if task.get('status') is None:
                    task['status'] = 'Incomplete'
                if task.get('args') is None:
                    task['args'] = []
                if task.get('priority') is None:
                    task['priority'] = 'NORMAL'
                if task.get('skip_to') is None:
                    task['skip_to'] = len(data['tasks'])
                if task.get('delay') is None:
                    task['delay'] = 0
                if task.get('retries') is None:
                    task['retries'] = 0
                if task.get('abort_type') is None:
                    task['abort_type'] = 'Panic'
                if task.get('immediate_next') is None:
                    task['immediate_next'] = False
            if data['repeat'] != 0 and data.get('repeat_interval') is None:
                print("Please specify repeat_interval")
                return None
            elif data['repeat'] == 0:
                data['repeat_interval'] = 0
            if data.get('time') is None:
                data['time'] = int(datetime.datetime.now(datetime.timezone.utc).timestamp())
            return data
    except OSError as err:
        print(f'Could not open file, {err=}')
