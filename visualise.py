from pyvis.network import Network
import webbrowser


def graph(goal):
    net = Network(directed=True)
    net.add_node('START')
    for i, task in enumerate(goal['tasks']):
        net.add_node(i, label=task['name'])
    net.add_node('FINISH')
    for i, task in enumerate(goal['tasks']):
        for n, pos in enumerate(task['next']):
            if pos >= len(goal['tasks']):
                net.add_edge(i, 'FINISH', label=n)
                continue
            net.add_edge(i, pos, label=n)
        if task.get('abort_type') and task['abort_type'] == "Skip":
            net.add_edge(i, task['skip_to'], label='recover')
    net.toggle_physics(False)
    net.add_edge('START', 0)
    net.show('vis.html')
    webbrowser.open(f'file://vis.html')
