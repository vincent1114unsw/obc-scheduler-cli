'''
main file
'''
import uuid
import validate
import json
import datetime
import transport
import sys
import visualise
START_MESSAGE = """OBC-SCHEDULER-CLI v0.1.0
"help" for list of commands, "quit" to quit'
Warning: Logs will be lost if you update a goal, please save logs by exporting first
Some fields may be ignored when you run add command, use update if you want to update a existing goal
"""
COMMAND_LIST = """List of commands:
- `add <filename.json>` --Adds a fresh goal json to memory queue
- `update <goal_id> <filename.json>` --Update an existing goal in memory
- `pop <goal_id>` --Delete an exisitng goal in memory
- `clear` --clears push queue
- `delete` --deletes a goal on the obc
- `list --verbose` --Lists all goals, verbose for more details
- `view <goal_id>` --Show details of a specific goal
- `logs <goal_id> <number>` --Show last <number> of logs of a specific goal
- `export <goal_id> <filename.json>` --Export a goal in memory as a json file
- `push` --Push memory queue to OBC redis server
- `pull` --Pull all goals from OBC redis server to memory
- `vis` visualise a goal either by json file or id"""


def main(auto_pull=False):
    '''
    Main function of the cli
    Loops command
    '''
    user_input = ''
    push_queue = {}
    goals_list = transport.pull_goals() if auto_pull else {}
    print('obc-scheduler-cli: "help" for list of commands, "quit" to quit')
    while user_input != 'quit':
        try:
            user_input = input("obc-scheduler-cli: ")
            command, args = user_input.split(" ")[0], user_input.split(" ")[1:]
            if command == "quit":
                break
            elif command == "help":
                print(COMMAND_LIST)
            elif command == "add":
                data = validate.compile_json(args[0])
                if data is not None:
                    push_queue[data['id']] = data
                    print(f"Added goal with id:{data['id']} to push queue, type `push` to upload goals to redis server")
            elif command == 'update':
                filename = args[0]
                data = validate.compile_json(filename, update=True)
                if data is not None:
                    push_queue[data['id']] = data
                    print(f"Added goal with id:{data['id']} to push queue, type `push` to upload goals to redis server")
            elif command == 'pop':
                og_len = len(push_queue)
                res = push_queue.pop(args[1], None)
                diff = og_len - len(push_queue)
                if diff == 1:
                    print(f"Poped goal:{args[1]} from push_queue")
                elif diff > 1:
                    print("Popped multiple goals from push_queue")
                elif res is None:
                    print("Could not pop goal, goal does not exist")
            elif command == 'clear':
                push_queue = {}
                print("Cleared push queue")
            elif command == 'delete':
                goal_id = args[0]
                force = "--force" in args
                if not force and goal_id not in goals_list:
                    print("Goal not in snapshot, please pull before deleting. Other wise specify --force to bypass this error")
                    continue
                push_queue[f"delete {goal_id}"] = f"delete {goal_id}"
                print(f"Added delete {goal_id} goal to push_queue")
            elif command == 'view':
                goal_id = args[0]
                exists = False
                if goal_id in push_queue:
                    exists = True
                    print("In push queue")
                    print(json.dumps(push_queue[goal_id], indent=4))
                if goal_id in goals_list:
                    exists = True
                    print("In snapshot")
                    print(json.dumps(goals_list[goal_id], indent=4))
                if not exists:
                    print("Goal id does not match any goal")
            elif command == 'logs':
                goal_id = args[0]
                num = 0 if len(args) < 2 else args[1]
                if goal_id in goals_list:
                    log_goal(goals_list[goal_id], num)
                else:
                    print("Goal not in snapshot")
            elif command == 'export':
                goal_id = args[0]
                if len(args) < 2:
                    filename = f'{goal_id}.json'
                else:
                    filename = args[1]
                exists = False
                if goal_id in push_queue and goal_id in goals_list:
                    print("obc-scheduler-cli: Id exists in both snapshot and push queue, which one do you want to export?")
                    print('1.snapshot  2.push queue  0.cancel')
                    choice = input()
                    if choice == '1':
                        dump_goal(goals_list[goal_id], filename)
                    elif choice == '2':
                        dump_goal(push_queue[goal_id], filename)
                    else:
                        continue

                if goal_id in push_queue:
                    dump_goal(push_queue[goal_id], filename)
                elif goal_id in goals_list:
                    dump_goal(goals_list[goal_id], filename)
                print(f"Exported to {filename}")
            elif command == 'list':
                print('In queue:')
                verbose = '--verbose' in args
                for goal in push_queue.values():
                    if type(goal) is str:
                        print("========")
                        print(goal)
                        print("========")
                        continue
                    if verbose:
                        print_verbose_goal(goal)
                    else:
                        print_simple_goal(goal)
                print("")
                print('Last Snapshot:')
                for goal in goals_list.values():
                    if verbose:
                        print_verbose_goal(goal)
                    else:
                        print_simple_goal(goal)
            elif command == 'push':
                for goal in push_queue.values():
                    if type(goal) is str:
                        print(goal)
                    else:
                        print(f'Pushing goal {goal["name"]}, id: {goal["id"]}')
                    transport.push_goal(goal)
                push_queue = {}
            elif command == 'pull':
                goals_list = transport.pull_goals()
                print(f'Successfully pulled {len(goals_list)} goals from obc server')
            elif command == 'vis':
                goal_id = args[0]
                exists = False
                if goal_id in push_queue:
                    exists = True
                    visualise.graph(push_queue[goal_id])
                elif goal_id in goals_list:
                    exists = True
                    visualise.graph(goals_list[goal_id])
                else:
                    with open(goal_id, encoding='utf-8') as json_file:
                        exists = True
                        data = json.load(json_file)
                        visualise.graph(data)
                if not exists:
                    print("Goal id does not match any goal")
            else:
                print("unknown command, type 'help' for a list of commands")
        except Exception as err:
            print(f"invalid input, please try again, {err=}")


def print_simple_goal(goal):
    '''Prints goal in non verbose manner'''
    print("========")
    print('name:', goal['name'])
    print('id:', goal['id'])
    print('status:', goal['status'])
    print('time:', datetime.datetime.utcfromtimestamp(goal['time']))
    print('number of tasks:', len(goal['tasks']))
    print('repeat:', goal['repeat'] if goal['repeat'] >= 0 else 'infinite')
    print("========")


def print_verbose_goal(goal):
    '''Print a goal showing everything in detail'''
    print("========")
    print(json.dumps(goal, indent=2))
    print("========", end="")


def dump_goal(goal, filename):
    '''dump goal to a json file'''
    with open(filename, 'w', encoding='utf-8') as f:
        json.dump(goal, f, ensure_ascii=False, indent=4)


def log_goal(goal, num):
    '''log goal in a nice way'''
    try:
        logs = goal['logs']
        for log in sorted(logs[-num:], key=lambda x: x['time']):
            print(f'[{log["log_type"]}] {datetime.datetime.utcfromtimestamp(int(log["time"]))}: {log["content"]}')
    except Exception as err:
        print(f"log failed, logs could be in a different format than expected, err={err}")


if __name__ == '__main__':
    auto_pull = '--autopull' in sys.argv

    main(auto_pull=auto_pull)
