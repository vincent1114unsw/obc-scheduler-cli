'''Transport functions between redis and cli
'''
import redis
import json

REDIS_URL = 'localhost'


def get_connection(addr: str):
    return redis.Redis(host=addr, port=6379, decode_responses=True)


def push_goal(goal):
    con = get_connection(REDIS_URL)
    if type(goal) is str:
        con.publish('goal_channel', goal)
        return
    con.publish('goal_channel', json.dumps(goal))


def pull_goals():
    res = {}
    con = get_connection(REDIS_URL)
    goal_keys = con.keys('goal:*')
    for key in goal_keys:
        json_string = con.execute_command('json.get', key, '.')
        goal = json.loads(json_string)
        res[goal['id']] = goal
    return res


if __name__ == '__main__':
    con = get_connection(REDIS_URL)
    b = con.pubsub()
    b.subscribe("obc-channel")
    print("listening to obc-channel")
    while True:
        data = b.get_message()
        if data is not None and 'data' in data:
            print(data['data'])
